/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.kittipon.banking;

/**
 *
 * @author kitti
 */
public class Test_Book_bank {
    public static void main(String[] args) {
        Book_bank book1 = new Book_bank();
        Book_bank book2 = new Book_bank();
        book1.name = "Cher_Tam";
        book1.balance = 84000;
        book2.name = "Cham_Ter";
        book2.balance = 250;     
        
//        book1 balance = book1 + 100;
        book1.deposit(100);
        System.out.println(book1.name + " " + book1.balance);
//        book1 balance = book1 - 1000;
        book1.withdraw(1000);
        System.out.println(book1.name + " " + book1.balance);
//        book2 balance = book2 + 50;
        book2.deposit(50);
        System.out.println(book2.name + " " + book2.balance); 
//        book2 balance = book2 - 1000;
        book2.withdraw(1000);
        System.out.println(book2.name + " " + book2.balance);      
    }
} 
